import java.util.*;
public class Primenumber {

    public static void main(String[] args)
    {

       int number,i,count=0;
        //getting input from user
        Scanner in=new Scanner(System.in);
        System.out.println("Enter a Number");
        number =in.nextInt();

        for (i = 2; i <= number/2; i++)
        {
            if(number % i == 0)
            {
                count++;
                break;
            }
        }
        if(count == 0 && number != 1 )
        {
            System.out.println( number + " is a Prime Number");
        }
        else
        {
            System.out.println(number + " is Not a Prime Number");
        }

    }
}
