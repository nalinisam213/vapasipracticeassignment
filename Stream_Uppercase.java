import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.List;

public class Stream_Uppercase {
    public static void main(String args[]) {

        List<String> strings = Arrays.asList("USA","Japan","France","Germany","India","Canada");

        System.out.println(strings.stream().map(s -> s.toUpperCase() ).collect(Collectors.joining(",")));
       // System.out.println(strings.stream().map());
    }

}
