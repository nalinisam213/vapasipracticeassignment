import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.List;

public class Stream_dist {

    public static void main(String args[]) {

        List<Integer> Integers = Arrays.asList(9, 10, 3, 7, 2, 3, 4);

        System.out.println(Integers.stream().distinct().map(i -> i * i).collect(Collectors.toList()));

    }
}
