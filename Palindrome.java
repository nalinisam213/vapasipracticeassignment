import java.util.Scanner;

public class Palindrome {

    public boolean Ispalindrome(String inputstring){
        String original= inputstring;
        String reverse= "";
        //Logic to reverse number
        int length= original.length();

        for(int i=length-1;i>=0;i--)
        {
            reverse = reverse+ original.charAt(i) ;
        }
        if (original.equals(reverse)){
            return true;
        }
        else
        {
            return false;
        }
    }

    public static void main(String[] args)
    {
        //getting input from user
        Scanner in=new Scanner(System.in);
        System.out.println("Enter the input");

        Palindrome palindrome = new Palindrome();
        boolean bresult= palindrome.Ispalindrome(in.nextLine());
        if(bresult)
            System.out.println("The given input is Palindrome");
        else
            System.out.println("The given input is not a Palindrome");

    }
}
