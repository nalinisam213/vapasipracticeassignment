

class Employee {
    String emp_name;
    int emp_id;

    public Employee(String emp_name, int emp_id) {
        this.emp_name = emp_name;
        this.emp_id = emp_id;
    }
}

class Manager extends Employee {
    int bonus;

    public Manager(String emp_name, int emp_id, int bonus) {
        super(emp_name, emp_id);
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        return "Manager{" +
                "emp_name='" + emp_name + '\'' +
                ", emp_id=" + emp_id +
                ", bonus=" + bonus +
                '}';
    }

}

public class Employee_Inheritance {
    public static void main(String args[]) {
        Manager manager = new Manager("Nalini", 123, 100);
        Manager manager1 = new Manager("samp", 345, 200);

        System.out.println(manager);
        System.out.println(manager1);
    }
}
