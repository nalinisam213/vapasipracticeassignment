
import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.List;


public class Stream_Lengthcut {

    public static void main(String args[]) {

        List<String> strings = Arrays.asList("sd", "sdd", "def");
        System.out.println(strings.stream().filter(s -> s.length() > 2).collect(Collectors.toList()));
    }

}