
 class Sum {
     public int addnumbers(int a, int b) {
         System.out.println("Adding Two Numbers");
         return a + b;
     }

     public int addnumbers(int a, int b, int c) {
         System.out.println("Adding Three Numbers");
         return a + b + c;
     }
 }

public class Moverload_ex {
    public static void main(String args[]) {
        Sum obj = new Sum();
        System.out.println( "Sum of the three Numbers is "+ obj.addnumbers(10,10,30));
        System.out.println( "Sum of the two Numbers is "+ obj.addnumbers(10,10));

    }
}
