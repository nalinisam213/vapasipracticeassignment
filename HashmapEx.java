import java.util.*;
public class HashmapEx {
    public static void main(String args[]) {
        Map<Integer, String> hashmap = new HashMap<Integer, String>();
        hashmap.put(1, "Red");
        hashmap.put(2, "Orange");
        hashmap.put(3, "Blue");
        hashmap.put(4, "Yellow");

        for (Map.Entry e : hashmap.entrySet()) {
            System.out.println(e.getKey()+" "+e.getValue());

        }
    }
}